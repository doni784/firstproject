package com.gm.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HelloResource 
{
	
	private static final Logger LOGGER=LoggerFactory.getLogger(HelloResource.class);
	@Autowired
	private RestTemplate resttemplate;
	@GetMapping(value="/client")
	public String hello()
	{
		LOGGER.info("Before calling");
	   /* we can directly call server name i.e hello-server instead of port no because of using eureka server*/
		String result=resttemplate.getForObject("http://hello-server/rest/hello", String.class);
		LOGGER.info("After Calling");
		return result;
	}

}
